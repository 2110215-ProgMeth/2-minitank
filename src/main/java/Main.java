import model.*;
import javafx.scene.paint.Color;

public class Main {
	private static Field f;

	public static void main(String[] args) {
		f = new Field();
		new Tank(f, 1, 1, Entity.EAST, 3, 3, 0, 5, Color.BLUE);
		new Tank(f, 3, 4, Entity.WEST, 3, 3, 0, 5, Color.RED);
		startGame();
	}

	public static void startGame() {
		while (true) {
			utility.GameScreen.startGame(f);
			f.updateFieldState();
		}
	}
}
