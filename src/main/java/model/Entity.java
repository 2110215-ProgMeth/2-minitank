package model;

public abstract class Entity {
	public static final int WEST=0,NORTH=1,EAST=2,SOUTH=3;
	protected int x,y,direction,nextX,nextY,nextDirection;
	private int movingDelay,movingDelayCounter;
	protected boolean isDestroyed,isDestroyedInNextState;
	protected Field field;
	public Entity(Field field, int x, int y, int direction, int movingDelay) {
		this.field = field;
		this.x = x;
		this.y = y;
		this.direction = direction;
		this.movingDelay = movingDelay;
		this.movingDelayCounter=movingDelay;
		this.field.addEntity(this);
		if(field.outOfField(x, y)){
			this.x=0;
			this.y=0;
		}
		if(direction<0||direction>3){
			this.direction = EAST;
		}
		calulateNextState();
	}
	public abstract void update();
	public abstract void calulateNextState();
	public boolean move(){
		if(isDestroyed)return false;
		if(isDestroyedInNextState){
			isDestroyed=true;
			return false;
		}
		boolean ret=false;
		if(movingDelayCounter==0){
			x=nextX;
			y=nextY;
			direction=nextDirection;
			movingDelayCounter=movingDelay;
			ret=true;
		}
		else movingDelayCounter--;
		calulateNextState();
		return ret;
	}
	public boolean isSamePosition(Entity other){
		return this.x==other.x && this.y==other.y;
	}
	public boolean isDestroyed() {
		return isDestroyed;
	}
	public void setDestroyed(boolean isDestroyed) {
		this.isDestroyed = isDestroyed;
	}
	public int getX() {
		return x;
	}
	public int getY() {
		return y;
	}
	public int getDirection() {
		return direction;
	}
	public int getNextX() {
		return nextX;
	}
	public int getNextY() {
		return nextY;
	}
	public int getNextDirection() {
		return nextDirection;
	}
	protected void genNextMove(){
		nextX=x;
		nextY=y;
		switch(direction){
		case EAST:
			nextX++;
			break;
		case NORTH:
			nextY--;
			break;
		case WEST:
			nextX--;
			break;
		case SOUTH:
			nextY++;
			break;
		}
	}
}
