package model;

public class Bullet extends Entity {
	protected Tank shooter;
	protected int power;

	public Bullet(Field field, int x, int y, int direction, int movingDelay, int power, Tank shooter) {
		super(field, x, y, direction, movingDelay);
		this.power = power;
		this.shooter = shooter;
	}

	public Tank getShooter() {
		return shooter;
	}

	private boolean canMove() {
		return field.getTerrainAt(nextX, nextY) == 0 || field.getTerrainAt(nextX, nextY) == -1;
	}

	@Override
	public void update() {
		if (isDestroyed)
			return;
		move();
	}

	@Override
	public void calulateNextState() {
		genNextMove();
		if (!canMove()) {
			nextX = x;
			nextY = y;
			isDestroyedInNextState = true;
		}
		nextDirection = direction;
	}

	public void hit() {
		for (Entity e : field.getEntities()) {
			if (e instanceof Tank && e.isSamePosition(this) && e != shooter) {
				isDestroyed = true;
				((Tank) e).decreaseLife(power);
			}
		}
	}
}
