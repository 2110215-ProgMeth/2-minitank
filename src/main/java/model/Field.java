package model;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class Field {
	private int[][] map={
		{0,0,0,0,0,0},
		{0,0,-2,0,0,0},
		{0,0,0,0,-1,0},
		{0,0,0,0,0,0},
		{0,-1,0,0,0,0},
		{0,0,0,0,0,0}
	};
	private List<Entity> entities;
	public Field(){
		entities=new CopyOnWriteArrayList<>();
	}
	public List<Entity> getEntities() {
		return entities;
	}
	public boolean outOfField(int x,int y){
		try{
			return map[y][x]!=map[y][x];
		}catch (ArrayIndexOutOfBoundsException e) {
			return true;
		}
	}
	public int getTerrainAt(int x,int y){
		if(outOfField(x, y))return -3;
		return map[y][x];
	}
	public void addEntity(Entity e){
		entities.add(e);
	}
	public void updateFieldState(){
//		entities.stream().filter(x->x.isDestroyed).forEach(x->entities.remove(x));
		for(int i=entities.size()-1;i!=0;i--){
			if(entities.get(i).isDestroyed)entities.remove(i);
		}
		entities.stream().forEach(x->x.update());
		entities.stream().filter(x->x instanceof Bullet).forEach(x->((Bullet)x).hit());
	}
}
