package model;

public class ExtraBullet extends Bullet {

	public ExtraBullet(Field field, int x, int y, int direction, int movingDelay, int power, Tank shooter) {
		super(field, x, y, direction, movingDelay, power, shooter);
		this.power = 2;
	}

	public void hit() {
		for (Entity e : field.getEntities()) {
			if (e instanceof Tank && e.isSamePosition(this) && e != shooter) {
				isDestroyed = true;
				((Tank) e).decreaseLife(power);
			}
			if (e instanceof Bullet && e.isSamePosition(this) && ((Bullet) e).getShooter() != shooter) {
				isDestroyed = true;
				((Bullet) e).isDestroyed = true;
			}
		}
	}
}
