package model;

import javafx.scene.paint.Color;

public class Tank extends Entity {
	private int life;
	private Color color;
	private int firingType, firingDelay, firingDelayCounter;

	public Tank(Field field, int x, int y, int direction, int movingDelay, int firingDelay, int firingType, int life,
			Color color) {
		super(field, x, y, direction, movingDelay);
		this.life = life > 0 ? life : 1;
		this.color = color == null ? Color.BLACK : color;
		this.firingDelay = firingDelay;
		this.firingDelayCounter = -1;
		this.firingType = firingType;
	}

	public int getLife() {
		return life;
	}

	public Color getTankColor() {
		return color;
	}

	public void decreaseLife(int amount) {
		life -= amount;
		if (life <= 0) {
			life = 0;
			isDestroyed = true;
		}
	}

	private boolean canMove() {
		return field.getTerrainAt(nextX, nextY) == 0;
	}

	private void randomTurn() {
		nextDirection = direction;
		nextDirection += (utility.Utility.random(0, 1) == 0) ? -1 : 1;
		nextDirection += 4;
		nextDirection %= 4;
	}

	@Override
	public void update() {
		if (isDestroyed)
			return;
		if (move()) {
			firingDelayCounter++;
		}
		if (firingDelayCounter == firingDelay) {
			if (firingType == 0) {
				new Bullet(field, x, y, direction, 1, 1, this);
			} else {
				new ExtraBullet(field, x, y, direction, 1, 2, this);
			}
			firingDelayCounter = -1;
		}
	}

	@Override
	public void calulateNextState() {
		isDestroyedInNextState = false;
		genNextMove();
		if (!canMove()) {
			randomTurn();
			nextX = x;
			nextY = y;
		} else {
			nextDirection = direction;
		}
	}
}
