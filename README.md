# Instruction

1. Fork this repository and clone it to your desktop or labtop.
2. **If you're using GitHub Desktop**, import this repository to your eclipse's workspace.
3. Add the external jar file `Lab2_lib.jar` to the project's build path .
4. Implement all classes following the details in `2110215_Lab2_2016.pdf` posted on CourseVille.
5. After finish the implementation, fill the answer in the worksheet.
6. Generate UML Class diagram using ObjecAid in Eclipse.
   * Your UML Class diagram must consists of all the classes inside package `model`
   * Save both ObjectAid ucls `uml.ucls` file and image as `uml.png` in project root directory 
7. Export your project into a runnable jar file with source codes named `Lab2_2016_{ID}.jar` where ID is your Student ID.
   * For example, `Lab2_2016_5770257521.jar`.
   * Make sure to export it into your project root directory.
8. Commit and Push it to your GitHub repository.
9. Send a Pull Request to us with Title: `[STUDENT-ID] Submission`.

# Deadline
Thursday 15th September 2016
